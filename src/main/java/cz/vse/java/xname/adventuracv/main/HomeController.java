package cz.vse.java.xname.adventuracv.main;

import cz.vse.java.xname.adventuracv.logika.Hra;
import cz.vse.java.xname.adventuracv.logika.IHra;
import cz.vse.java.xname.adventuracv.logika.Prostor;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

import java.util.HashMap;
import java.util.Map;

public class HomeController extends GridPane implements Observer {

    @FXML private ImageView hrac;
    @FXML private ListView panelVychodu;
    @FXML private TextArea vystup;
    @FXML private TextField vstup;
    @FXML private Button odesli;

    private IHra hra;
    Map<String,Point2D> souradnice = new HashMap<>();

    @FXML
    private void initialize() {
        hra = new Hra();
        vystup.appendText(hra.vratUvitani()+"\n\n");
        panelVychodu.getItems().addAll(hra.getHerniPlan().getAktualniProstor().getVychody());

        hra.getHerniPlan().register(this);

        souradnice = createSouradnice();

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                vstup.requestFocus();
            }
        });
    }

    @FXML
    private void zpracujVstup(ActionEvent actionEvent) {
        String prikaz = vstup.getText();
        zpracujPrikaz(prikaz);
        vstup.clear();
    }

    private void zpracujPrikaz(String prikaz) {
        vystup.appendText("Příkaz: "+prikaz+"\n");
        String vysledek = hra.zpracujPrikaz(prikaz);
        vystup.appendText(vysledek+"\n\n");

        if(hra.konecHry()) {
            vystup.appendText(hra.vratEpilog());
            vstup.setDisable(true);
            odesli.setDisable(true);
            panelVychodu.setDisable(true);
        }
    }

    @Override
    public void update() {
        Prostor aktualniProstor = hra.getHerniPlan().getAktualniProstor();

        panelVychodu.getItems().clear();
        panelVychodu.getItems().addAll(aktualniProstor.getVychody());

        hrac.setLayoutX(souradnice.get(aktualniProstor.getNazev()).getX());
        hrac.setLayoutY(souradnice.get(aktualniProstor.getNazev()).getY());
    }

    private Map<String, Point2D> createSouradnice() {
        Map<String, Point2D> souradnice = new HashMap<>();
        souradnice.put("domeček",new Point2D(14,68));
        souradnice.put("les",new Point2D(79,37));
        souradnice.put("hluboký_les",new Point2D(143,76));
        souradnice.put("chaloupka",new Point2D(206,36));
        souradnice.put("jeskyně",new Point2D(145,145));
        return souradnice;
    }

    public void vybranVychod(MouseEvent mouseEvent) {
        Prostor vybranyProstor = (Prostor) panelVychodu.getSelectionModel().getSelectedItem();
        if(vybranyProstor!=null) {
            String prikaz = "jdi " + vybranyProstor.getNazev();
            zpracujPrikaz(prikaz);
        }
    }
}
