package cz.vse.java.xname.adventuracv.main;

public interface Observer {
    /**
     * metoda, kterou volá předmět pozorování (subject) při změně
     */
    void update();
}
